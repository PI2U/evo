package de.pi2u.evo;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "EVO 1337";
		cfg.width = 1280;
		cfg.useGL20 = true;
		cfg.fullscreen = false;
		cfg.height = 720;
	    cfg.vSyncEnabled = true;
		new LwjglApplication(new Game(), cfg);
	}
}
