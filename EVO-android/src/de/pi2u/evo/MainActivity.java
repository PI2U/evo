package de.pi2u.evo;

import android.os.Bundle;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

public class MainActivity extends AndroidApplication {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
        ApplicationListener game = new Game();
        cfg.useAccelerometer = false;
        cfg.useCompass = false;
        cfg.useGL20 = true;

        initialize(game, cfg);
    }
}