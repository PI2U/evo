#ifdef GL_ES
precision mediump float;
#endif

/*
#define M_PI 3.1415926535897932384626433832795

varying vec2 v_texCoords;
varying float v_time;
uniform sampler2D u_texture;


void main()
{
  vec4 textureColor = texture2D(u_texture, v_texCoords).rgba;

  float rgba = abs( sin( radians(v_time) ) );
  gl_FragColor =  vec4(rgba,0.0,1.0-rgba,textureColor.a);
}
*/

varying float v_time;
varying vec2 v_texCoords;
uniform sampler2D u_texture;
uniform vec2 resolution;
uniform float hoehe;

void main( void ) {

	vec2 p = ( gl_FragCoord.xy / vec2(resolution.xy) ) - 0.5;
	float sx = hoehe * (p.x + 0.5) * sin( 24.0 * p.x - 10.0 * radians(v_time) );
	float dy = 1.0 / ( 50.0 * abs(p.y - sx));
	dy += 1.0 / (20.0 * length(p - vec2(p.x, 0.)));
	gl_FragColor = texture2D(u_texture, v_texCoords).rgba * vec4( (p.x + 0.1) * dy, 0.1 * dy, dy * 2.0, 1.0 );
	
}