#ifdef GL_ES
precision mediump float;
#endif

uniform float time;
uniform vec2 mouse;
uniform vec2 resolution;
uniform vec2 pos;



void main( void ) {


float posy = resolution.y-pos.y;
	
	vec2 position =  gl_FragCoord.xy/vec2(resolution.x,resolution.y+posy*4.) ;
	float tmp = 0.005*sin( position.x*20. + time ) +0.105;
	float tmp2 = 0.005*sin( position.x*20. + time/1.5 ) +0.1;
	float tmp3 = 0.005*sin( position.x*20. + time/2. ) +0.1;
	vec4 color = vec4(0.0,0.0,0.0,0.0);

  			
			if(position.y <= (tmp) ) {
				color = vec4(0.0,0.0,1.0,1.0);
       		 }
  
			if(position.y <= (tmp2)){
				color = vec4(0.2,0.3,1.0,1.0);
       		 }
  
			if(position.y <= (tmp3)  ){
				color = vec4(0.0,0.5,1.0,1.0);
        	}
     
  
	gl_FragColor = color;
}