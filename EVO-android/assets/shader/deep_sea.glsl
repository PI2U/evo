#ifdef GL_ES
precision lowp float;
#endif

uniform  float time;
uniform vec2 resolution;
uniform vec2 pos;


void main( void ) {
  
float s = 0.0;
vec2 o;
  
vec2 p=vec2(gl_FragCoord.x/(resolution.x),gl_FragCoord.y/(resolution.y));
		   
for(float i=0.0;i<40.0;i+=2.5){

  
	
	
	o= vec2( i+(sin(time/2.))*10.,abs(cos(i+time)*40.) );
	s += 0.005/length(p-o);
}

vec3 c=s*vec3(0.5,0.9,1.5);
gl_FragColor= vec4( 0.2*p.y  ,0.2*p.y,1.0*p.y, 1.0) *  vec4(c.x,c.y,c.z,1.0);

}