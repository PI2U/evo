package de.pi2u.evo;

import com.badlogic.gdx.graphics.Pixmap;

public class Util {

	public Util() {
		// TODO Auto-generated constructor stub
	}

	public static int pixelAtPoint( int x , int y , Pixmap pix ) {
		int pixel = pix.getPixel(x, y);
		return pixel;
	}
	
	public static boolean checkCollision( Pixmap input, int x , int y ) {
		

		if ((pixelAtPoint(x, y, input) & 0x000000ff) != 0) {
			return true;
		}


		return false;
	}
	
	public static int checkPlayerMonsterCollision(Player player, Monster monster){
		
		//System.out.println(monster.y + " " + player.y);
		if( ( player.x >= monster.x && player.x <= monster.x+Monster.TILESIZE ) || (player.x+player.TILESIZE  >= monster.x && player.x+player.TILESIZE <= monster.x+Monster.TILESIZE) ){
			if( ( player.y >= monster.y && player.y <= monster.y+Monster.TILESIZE ) || (player.y+player.TILESIZE  >= monster.y && player.y+player.TILESIZE <= monster.y+Monster.TILESIZE) ){
				return monster.id;
			}
		}
		
		return 0;	
	}
	
	public static boolean checkMonsterMonsterCollision(Monster player, Monster monster){
		
		//System.out.println(monster.y + " " + player.y);
		if( ( player.x >= monster.x && player.x <= monster.x+Monster.TILESIZE ) || (player.x+player.TILESIZE  >= monster.x && player.x+player.TILESIZE <= monster.x+Monster.TILESIZE) ){
			if( ( player.y >= monster.y && player.y <= monster.y+Monster.TILESIZE ) || (player.y+player.TILESIZE  >= monster.y && player.y+player.TILESIZE <= monster.y+Monster.TILESIZE) ){
				return true;
			}
		}
		
		return false;	
	}
	
}
