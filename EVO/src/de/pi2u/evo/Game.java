package de.pi2u.evo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

import com.badlogic.gdx.*;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.utils.TimeUtils;

public class Game implements ApplicationListener {
	static int									WIDTH;
	static int									HEIGHT;

	static float								DEADZONE;
	static final int							COLLISION_NONE			= 0;
	static final int							COLLISION_LINKS_UNTEN	= 1;
	static final int							COLLISION_RECHTS_UNTEN	= 2;
	static final int							COLLISION_LINKS_OBEN	= 4;
	static final int							COLLISION_RECHTS_OBEN	= 8;
	static final int							COLLISION_RECHTS		= 16;
	static final int							COLLISION_LINKS			= 32;
	static final int							COLLISION_OBEN			= 64;
	static final int							COLLISION_UNTEN			= 128;
	static final int							WATER_HEIGHT			= 1500;

	int											SPEED					= 10;
	int											JOYSTICK_X;
	int											JOYSTICK_Y;
	final int									JOYSTICK_SIZE			= 128;

	public OrthographicCamera					cam;
	public Texture								backgroundTexture, water, ground, joystick;
	public SpriteBatch							background, waterbatch, overlay, entity;
	public static float							camX, camY;
	public static int							worldSizeY, worldSizeX;
	public BitmapFont							font;
	public static Random						rnd;
	public Pixmap								groundPixmap;
	public static Player						player;
	public int									collision;

	public static HashMap<String, Texture>		texturelist				= new HashMap<String, Texture>();
	public static HashMap<String, Animation>	animationlist			= new HashMap<String, Animation>();
	public boolean								up, down, left, right, attack;
	public ShaderProgram						shader;
	public long									timeStart;


	@Override
	public void create() {

		WIDTH = Gdx.graphics.getWidth();
		HEIGHT = Gdx.graphics.getHeight();
		JOYSTICK_X = (int) (WIDTH * 0.1);
		JOYSTICK_Y = HEIGHT / 2;
		DEADZONE = HEIGHT * 0.1f;

		backgroundTexture = new Texture(Gdx.files.internal("textures/background.png"));
		water = new Texture(Gdx.files.internal("textures/water.png"));
		joystick = new Texture(Gdx.files.internal("ui/joystick.png"));
		groundPixmap = new Pixmap(Gdx.files.internal("textures/ground.png"));
		ground = new Texture(groundPixmap);

		cam = new OrthographicCamera(WIDTH, HEIGHT);
		cam.position.set((WIDTH / 2), (HEIGHT / 2), 0);

		worldSizeY = backgroundTexture.getHeight();
		worldSizeX = backgroundTexture.getWidth();

		background = new SpriteBatch(10);
		waterbatch = new SpriteBatch(10);
		overlay = new SpriteBatch(100);
		entity = new SpriteBatch(100);

		font = new BitmapFont(Gdx.files.internal("ui/font.fnt"));

		rnd = new Random();
		player = new Player((WIDTH / 2) - 32, (HEIGHT / 2) - 32);

		ShaderProgram.pedantic = false;
		shader = new ShaderProgram(Gdx.files.internal("shader/vertex.shader"), Gdx.files.internal("shader/wellen.glsl"));
		if (!shader.isCompiled()) {
			Gdx.app.log("Shader", shader.getLog());
		}

		FileHandle handle = Gdx.files.internal("data/dc.txt");
		String[] fileContent = handle.readString().split("\n");

		for (int i = 0; i < 10000; i++) {
			int x = (rnd.nextInt() % worldSizeX);
			int y = (rnd.nextInt() % worldSizeY);

			if (!Util.checkCollision(groundPixmap, x, worldSizeY - y) && !Util.checkCollision(groundPixmap, x, worldSizeY - (y + player.TILESIZE)) && !Util.checkCollision(groundPixmap, x + player.TILESIZE, worldSizeY - y) && !Util.checkCollision(groundPixmap, x + player.TILESIZE, worldSizeY - (y + player.TILESIZE)) && y < WATER_HEIGHT - Monster.TILESIZE) {
				new Monster(x, y, fileContent);
			}

		}

		handle = Gdx.files.internal("data/monster.txt");
		fileContent = handle.readString().split("\n");

		for (int i = 0; i < 10000; i++) {
			int x = Math.abs((rnd.nextInt() % worldSizeX));
			int y = Math.abs((rnd.nextInt() % worldSizeY));

			if (!Util.checkCollision(groundPixmap, x, worldSizeY - y) && !Util.checkCollision(groundPixmap, x, worldSizeY - (y + player.TILESIZE)) && !Util.checkCollision(groundPixmap, x + player.TILESIZE, worldSizeY - y) && !Util.checkCollision(groundPixmap, x + player.TILESIZE, worldSizeY - (y + player.TILESIZE)) && y < WATER_HEIGHT - Monster.TILESIZE) {
				new Monster(x, y, fileContent);
			}

		}

		timeStart = (long) (TimeUtils.millis() - 1000);
	}


	@Override
	public void render() {
		Monster.updateStateTime();
		handleInput();

		// What faces to remove with the face culling.
		Gdx.gl.glCullFace(GL10.GL_BACK);

		cam.update();

		camX = cam.position.x;
		camY = cam.position.y;

		Gdx.gl.glClearColor(0, 0, 0, 0);
		player.update();

		background.setProjectionMatrix(cam.combined);
		background.begin();
		float time = (float) (TimeUtils.millis() - timeStart) / 500;
		background.draw(backgroundTexture, 0, 0);
		background.end();

		waterbatch.begin();
		shader.setUniformf("time", time);
		shader.setUniformf("pos", cam.position.x, cam.position.y);
		shader.setUniformf("resolution", worldSizeX, worldSizeY);
		waterbatch.setShader(shader);
		waterbatch.setColor(1.0f, 1.0f, 1.0f, 0.5f);
		waterbatch.draw(water, 0, 0, worldSizeX, WATER_HEIGHT + 256);

		waterbatch.end();

		entity.setProjectionMatrix(cam.combined);
		entity.begin();
		entity.draw(ground, 0, 0);
		player.draw(entity);
		Monster.update(entity);

		entity.end();

		overlay.begin();
		font.draw(overlay, String.valueOf(Gdx.graphics.getFramesPerSecond()), 00, HEIGHT);
		font.draw(overlay, "HP " + player.health, 100, HEIGHT);
		font.draw(overlay, "EVOPOINTS " + player.evoPoints, 300, HEIGHT);
		
		overlay.setColor(1, 1, 1, 128);
		overlay.draw(joystick, JOYSTICK_X - JOYSTICK_SIZE / 2, HEIGHT - (JOYSTICK_Y + JOYSTICK_SIZE / 2));
		overlay.setColor(255, 0, 0, 128);
		overlay.draw(joystick, WIDTH - JOYSTICK_SIZE, 0);

		overlay.end();

	}


	private void handleInput() {

		up = down = left = right = attack = false;

		if (Gdx.input.justTouched()) {
			JOYSTICK_X = Gdx.input.getX();
			JOYSTICK_Y = Gdx.input.getY();
		}

		if (Gdx.input.isTouched()) {

			collision = player.checkCollision(groundPixmap);

			if (Gdx.input.getX() >= JOYSTICK_X + DEADZONE) {
				right = true;
			}
			else if (Gdx.input.getX() <= JOYSTICK_X - DEADZONE) {
				left = true;
			}

			if (Gdx.input.getY() <= JOYSTICK_Y - DEADZONE) {
				up = true;
			}
			else if (Gdx.input.getY() >= JOYSTICK_Y + DEADZONE) {
				down = true;
			}

			if (Gdx.input.getY() >= JOYSTICK_SIZE && Gdx.input.getX() >= WIDTH - JOYSTICK_SIZE) {

				attack = true;

			}
		}

		if (Gdx.input.isKeyPressed(Input.Keys.ANY_KEY)) {

			collision = player.checkCollision(groundPixmap);

			if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
				left = true;
			}
			if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
				right = true;
			}
			if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
				up = true;
			}
			if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
				down = true;
			}
			if (Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
				attack = true;
			}
		}

		if (Gdx.input.isTouched() || Gdx.input.isKeyPressed(Input.Keys.ANY_KEY)) {
			if (!left && (collision & (COLLISION_RECHTS_UNTEN | COLLISION_RECHTS)) != 0) {

				down = false;

				if (right && (collision & COLLISION_RECHTS) == 0) {
					up = true;
				}
				else {
					right = false;
					up = true;
				}
			}

			if ((collision & (COLLISION_LINKS_UNTEN | COLLISION_LINKS)) != 0) {

				down = false;

				if (left && (collision & COLLISION_LINKS) == 0) {
					up = true;

				}
				else {
					left = false;
					up = true;
				}

			}

			if ((collision & (COLLISION_OBEN)) != 0) {

				if (left && (collision & COLLISION_OBEN) == 0) {
					down = true;

				}
				else {

					down = true;
				}

			}

			if ((collision & (COLLISION_UNTEN)) != 0) {

				down = false;
			}

			if ((collision & (COLLISION_OBEN)) != 0) {

				System.out.println("COLLISION_OBEN");
				down = true;
				up = false;
			}

			if (left) {

				if (cam.position.x > (WIDTH / 2) + SPEED && player.x + (player.TILESIZE / 2) <= worldSizeX - (WIDTH / 2)) {
					cam.translate(-SPEED, 0);
				}
				if (player.x > 0) {
					player.moveDelta(-SPEED, 0);
				}

				player.flip = false;

			}

			if (right) {

				if (cam.position.x < worldSizeX - (WIDTH / 2) - SPEED && player.x + (player.TILESIZE / 2) >= (WIDTH / 2)) {
					cam.translate(SPEED, 0);
				}
				if (player.x < (worldSizeX - player.TILESIZE)) {
					player.moveDelta(SPEED, 0);
				}

				player.flip = true;

			}

			if (down) {

				if (cam.position.y > (HEIGHT / 2) && player.y + (player.TILESIZE / 2) <= worldSizeY - (HEIGHT / 2)) {
					cam.translate(0, -SPEED);
				}
				if (player.y > 0) {
					player.moveDelta(0, -SPEED);
				}
				else {
					player.y = 0;
				}

			}

			if (up) {

				if (player.y <= WATER_HEIGHT - player.TILESIZE) {
					if (cam.position.y <= worldSizeY - (HEIGHT / 2) - SPEED && player.y + (player.TILESIZE / 2) >= (HEIGHT / 2)) {
						cam.translate(0, SPEED);
					}
					if (player.y < worldSizeY - player.TILESIZE) {
						player.moveDelta(0, SPEED);
					}
					else {
						player.y = worldSizeY - player.TILESIZE;
					}
				}

			}

			if (attack) {
				player.attack();

			}
		}
	}


	@Override
	public void resize( int width , int height ) {

	}


	@Override
	public void resume() {

	}


	@Override
	public void dispose() {

		for (Monster e : Monster.monsterList) {
			e.monster.dispose();

		}
		groundPixmap.dispose();
		backgroundTexture.dispose();
		water.dispose();
		ground.dispose();
		waterbatch.dispose();
		overlay.dispose();
		entity.dispose();
		player.player.dispose();
		background.dispose();
		texturelist.clear();
		animationlist.clear();
		try {
			finalize();
		}
		catch (Throwable e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("ENDE");
	}


	@Override
	public void pause() {
		System.out.println("PAUSE");
	}
}
