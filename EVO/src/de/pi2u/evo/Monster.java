package de.pi2u.evo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Monster {

	private final int			FRAME_COLS		= 2;
	private final int			FRAME_ROWS		= 2;

	private final int			STATE_IDLE		= 0;
	private final int			STATE_ATTACK	= 1;
	public static final int		TILESIZE		= 64;
	public static List<Monster>	monsterList		= new CopyOnWriteArrayList<Monster>();

	public Animation			monsterAnimation;
	public int					id;
	public static int			lastid;
	public TextureRegion		currentMonsterFrame;
	public static float			stateTime;
	public Texture				monster;
	public int					state			= STATE_IDLE, x, y, worldX, worldY,evoPoints,damage;
	public boolean				flip;


	public Monster(int x, int y, String[] texture) {

		this.x = x;
		this.y = y;

		for (Monster e : monsterList) {
			if (Util.checkMonsterMonsterCollision(this, e)) {

				return;
			}
		}

		TextureRegion[] monsterFrames;

		if (Game.texturelist.containsKey(texture[0])) {

			monster = Game.texturelist.get(texture[0]);
		}
		else {
			System.out.println("Loading Texture " + texture[0]);
			monster = new Texture(Gdx.files.internal(texture[0].trim()));
			Game.texturelist.put(texture[0], monster);
		}
		if (Game.animationlist.containsKey(texture[0] + "animation")) {

			monsterAnimation = Game.animationlist.get(texture[0] + "animation");
		}
		else {
			System.out.println("Loading " + texture[0] + "animation");

			TextureRegion[][] tmp = TextureRegion.split(monster, monster.getWidth() / FRAME_COLS, monster.getHeight() / FRAME_ROWS);

			monsterFrames = new TextureRegion[FRAME_COLS * FRAME_ROWS];

			int index = 0;
			for (int i = 0; i < FRAME_ROWS; i++) {
				for (int j = 0; j < FRAME_COLS; j++) {
					monsterFrames[index++] = tmp[i][j];
				}
			}

			monsterAnimation = new Animation(0.25f, monsterFrames);

			Game.animationlist.put(texture[0] + "animation", monsterAnimation);
		}

		if (Game.rnd.nextInt(100) >= 50) {
			flip = true;
		}
		else {
			flip = false;
		}

		worldX = x;
		worldY = Game.worldSizeY - y;
		
		evoPoints = Integer.parseInt(texture[1].trim());
		damage = Integer.parseInt(texture[2].trim());
		

		monsterList.add(this);
		id = lastid++;

	}


	public static void updateStateTime() {
		stateTime += Gdx.graphics.getDeltaTime();

	}


	public void update() {
		if (state == STATE_IDLE) {

			currentMonsterFrame = monsterAnimation.getKeyFrame(stateTime, true);

		}
		else if (state == STATE_ATTACK) {
			currentMonsterFrame = monsterAnimation.getKeyFrame(0, false);
			state = STATE_IDLE;
		}

	}


	public void draw( SpriteBatch target ) {
		if (x >= Game.camX - (Game.WIDTH / 2) - TILESIZE && x <= Game.camX + Game.WIDTH / 2) { // View
																								// frustum
																								// culling
			update();

			if (y >= Game.camY - (Game.HEIGHT / 2) - TILESIZE && y <= Game.camY + Game.HEIGHT / 2) {
				if (flip) {
					target.draw(currentMonsterFrame, x, y, TILESIZE, TILESIZE);
				}
				else {
					target.draw(currentMonsterFrame, x + TILESIZE, y, -TILESIZE, TILESIZE);
				}
			}
		}
	}


	public void moveDelta( int x , int y ) {
		this.x += x;
		this.y += y;

	}


	public void attack() {

		if (state != STATE_ATTACK) {
			state = STATE_ATTACK;
		}
	}


	public static void update( SpriteBatch entity ) {
		int id;

		for (Monster e : monsterList) {
			e.draw(entity);
			if ((id = Util.checkPlayerMonsterCollision(Game.player, e)) > 0) {
				System.out.println("Kollision mit Monster " + id);
				monsterList.remove(e);
				Game.player.evoPoints += e.evoPoints;
				
			}
		}

	}

}
