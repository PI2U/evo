package de.pi2u.evo;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Player {

	private final int		FRAME_COLS		= 2;
	private final int		FRAME_ROWS		= 2;

	private final int		STATE_IDLE		= 0;
	private final int		STATE_ATTACK	= 1;
	public final int		TILESIZE		= 64;

	public Animation		playerAnimation;
	public TextureRegion[]	playerFrames;
	public TextureRegion	currentPlayerFrame;
	public float			stateTime;
	public Texture			player;
	public int				state			= STATE_IDLE, x, y, worldX, worldY;
	public boolean			flip;
	public int health = 100, evoPoints;

	public Player(int x, int y) {

		player = new Texture(Gdx.files.internal("textures/player.png"));

		TextureRegion[][] tmp = TextureRegion.split(player, player.getWidth() / FRAME_COLS, player.getHeight() / FRAME_ROWS);

		playerFrames = new TextureRegion[FRAME_COLS * FRAME_ROWS];

		int index = 0;
		for (int i = 0; i < FRAME_ROWS; i++) {
			for (int j = 0; j < FRAME_COLS; j++) {
				playerFrames[index++] = tmp[i][j];
			}
		}

		playerAnimation = new Animation(0.15f, playerFrames);

		this.x = x;
		this.y = y;

		worldX = x;
		worldY = Game.worldSizeY - y;

	}


	public int checkCollision( Pixmap input ) {
		int collision = Game.COLLISION_NONE;

		if ((Util.pixelAtPoint(worldX, worldY, input) & 0x000000ff) != 0) {
			collision |= Game.COLLISION_LINKS_UNTEN;
		}
		if ((Util.pixelAtPoint(worldX, worldY - TILESIZE, input) & 0x000000ff) != 0) {
			collision |= Game.COLLISION_LINKS_OBEN;
		}
		if ((Util.pixelAtPoint(worldX + TILESIZE, worldY, input) & 0x000000ff) != 0) {
			collision |= Game.COLLISION_RECHTS_UNTEN;
		}
		if ((Util.pixelAtPoint(worldX + TILESIZE, worldY - TILESIZE, input) & 0x000000ff) != 0) {
			collision |= Game.COLLISION_RECHTS_OBEN;
		}

		if ((Util.pixelAtPoint(worldX + (TILESIZE / 2), worldY, input) & 0x000000ff) != 0) {
			collision |= Game.COLLISION_UNTEN;
		}
		if ((Util.pixelAtPoint(worldX + (TILESIZE / 2), worldY - TILESIZE, input) & 0x000000ff) != 0) {
			collision |= Game.COLLISION_OBEN;
		}
		if ((Util.pixelAtPoint(worldX + TILESIZE, worldY - (TILESIZE / 2), input) & 0x000000ff) != 0) {
			collision |= Game.COLLISION_RECHTS;
		}
		if ((Util.pixelAtPoint(worldX, worldY - (TILESIZE / 2), input) & 0x000000ff) != 0) {
			collision |= Game.COLLISION_LINKS;
		}

		return collision;
	}


	
	public void update() {
		if (state == STATE_ATTACK) {

			stateTime += Gdx.graphics.getDeltaTime();
			if (playerAnimation.isAnimationFinished(stateTime)) {
				state = STATE_IDLE;
				stateTime = 0;
			}
			currentPlayerFrame = playerAnimation.getKeyFrame(stateTime, false);

		}
		else {
			currentPlayerFrame = playerAnimation.getKeyFrame(0, false);
			state = STATE_IDLE;
		}

	}


	public void draw( SpriteBatch target ) {
		if (flip) {
			target.draw(currentPlayerFrame, x, y, TILESIZE, TILESIZE);
		}
		else {
			target.draw(currentPlayerFrame, x + TILESIZE, y, -TILESIZE, TILESIZE);
		}

	}


	public void moveDelta( int x , int y ) {
		this.x += x;
		this.y += y;
		worldX = this.x;
		worldY = Game.worldSizeY - this.y;
	}


	public void attack() {

		if (state != STATE_ATTACK) {
			state = STATE_ATTACK;
		}
	}
}
